var express = require('express');
var router = express.Router();

module.exports =  (app) => {
"use strict";

  return router.get('/:id', (req, res, next) => {
    const db = app.get('db');
    db.disaster.weather.findOne(+req.params.id)
      .then((result) => 
      {
        return (!result) ?  res.status(404).send('404 no result') : res.render('index', {
            title: '/latest_data/:' + req.params.id,
            entry: result
        });
      }, (err) => {next(err);})
  });
};
