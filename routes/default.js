var express = require('express');
var router = express.Router();

module.exports = function(app){
    "use strict";
    router.get('/', function(req, res, next) {
        res.render('default', { title: 'Weather Api'});
    });
    return router;
};