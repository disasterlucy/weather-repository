var express = require('express');
var router = express.Router();

module.exports = function(app){
    "use strict";
    router.get('/DataBase/:stage([0-9]+)', function(req, res, next) {
        const db = app.get('db');

        if(db === undefined) {
            var err = new Error('No DB connected');
            err.name = 'DataBase error';
            next(err);
        }
        else {
            var strTitle = "DataBase",
                strSubTitle = "",
                strSubMessage = "",
                myErr;

            if(req.params.stage == 0){ //default
                strSubTitle = "Info:";

                db.run("SELECT Count(*) FROM information_schema.schemata WHERE schema_name = 'lucy';")
                  .then(function (resQuerySchema) {
                      if(resQuerySchema[0].count == 0){
                        strSubMessage = "Schema: not initialized\nDataBase: not initialized";
                        res.render('dataBase', {title: strTitle, subTitle: strSubTitle, subMessage: strSubMessage});
                      }
                      else {
                        db.run("SELECT COUNT(*) FROM pg_tables WHERE schemaname='lucy';")
                            .then(function (resQueryTable) {
                                if(resQueryTable[0].count == 0){
                                    strSubMessage = "Schema: initialized\nDataBase: not initialized";
                                } else {
                                    strSubMessage = "Schema: initialized\nDataBase: initialized";
                                }
                                res.render('dataBase', {title: strTitle, subTitle: strSubTitle, subMessage: strSubMessage});
                            });
                      }
                });
            } else if(req.params.stage == 1) { //init
                strSubTitle = "DataBase Initialization:";
                strSubMessage = "Schema: initialized\nDataBase: initialized";

                db.run("SELECT Count(*) FROM information_schema.schemata WHERE schema_name = 'lucy';")
                    .then(function (resQuerySchema) {
                        if(resQuerySchema[0].count == 0){
                            db.run("CREATE SCHEMA lucy;")
                                .then(function (resCSchedule) {
                                    db.run(
                                           "CREATE TABLE IF NOT EXISTS lucy.weather ("     +
                                           "id SERIAL PRIMARY KEY NOT NULL," +
                                           "date TIMESTAMP,"                 +
                                           "city VARCHAR(100),"              +
                                           "data JSON);"
                                          );
                                });
                        } else {
                            db.run(
                                "CREATE TABLE IF NOT EXISTS lucy.weather ("     +
                                "id SERIAL PRIMARY KEY NOT NULL," +
                                "date TIMESTAMP,"                 +
                                "city VARCHAR(100),"              +
                                "data JSON);"
                                );
                        }
                        res.render('dataBase', {title: strTitle, subTitle: strSubTitle, subMessage: strSubMessage});
                    });
            } else if(req.params.stage == 2) { //remove
                strSubTitle = "DataBase Deleting:";

                db.run("SELECT COUNT(*) FROM pg_tables WHERE schemaname='lucy';")
                    .then(function (resQueryTable) {
                        if(resQueryTable[0].count != 0){
                            strSubMessage = "Schema: deleted\nDataBase: deleted";
                            db.run("DROP TABLE lucy.weather;").then( function (resDTable) {
                                db.run("DROP SCHEMA lucy;").then( function (resDSchedule) {
                                    res.render('dataBase', {title: strTitle, subTitle: strSubTitle, subMessage: strSubMessage});
                                });
                            });
                        }
                    });

                db.run("SELECT COUNT(*) FROM information_schema.schemata WHERE schema_name = 'lucy';")
                    .then(function (resQuerySchema) {
                        if(resQuerySchema[0].count != 0){
                            strSubMessage = "Schema: deleted";
                            db.run("DROP SCHEMA lucy;").then( function (resDSchedule) {
                                res.render('dataBase', {title: strTitle, subTitle: strSubTitle, subMessage: strSubMessage});
                            });
                        } else {
                            strSubMessage = "All Data already deleted";
                            res.render('dataBase', {title: strTitle, subTitle: strSubTitle, subMessage: strSubMessage});
                        }
                });
            }
        }
    });
    return router;
};
