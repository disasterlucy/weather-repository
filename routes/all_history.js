var express = require('express');
var router = express.Router();

module.exports =  (app) => {

  return router.get('/', (req, res, next) => {
    const db = app.get('db');
    db.disaster.weather.find()
      .then((result) => 
      { 
        //res.render('all_history', {  cityData: result});
        return (!result) ? res.status(404).send() : res.json(result);  //Или можно через res.render и обработать вывод в ejs
      }, (err) => {next(err);})
  }); 
};
