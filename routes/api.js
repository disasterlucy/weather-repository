var express = require('express');
var router = express.Router();
var apiGW = require('../modules/apiGetWeather');

module.exports = (app) => {

    return router.get('/:city', (req, res, next) => {   
      apiGW.getWeather(req.params['city'])
      .then((data) => {
        const db = app.get('db'); 
        db.disaster.weather.insert({  //date = now() in db          
            city: req.params['city'],
            data: data
          })
        res.send("Success!");
      }, err => next(err))
    });
};
