const requestPromise = require('request-promise');

function getData(city) {

    var options = {
        url: 'http://api.openweathermap.org/data/2.5/weather?q=' + city + '&appid=22fd561cdc8b84851a43b8fe3405053d',
        json: true,  // Automatically parses the JSON string in the response 
        simple: false,  //Get a rejection only if the request failed for technical reasons
        resolveWithFullResponse: true  //Get the full response 
    }
    
    return requestPromise(options).then((res) => {
        if (res.statusCode !== 200) throw new Error(res.statusCode);
        return res.body;
    });
}

function processData(data) {

    return data.weather.reduce((prev, cur) => {return prev + cur.main + ', ';}, data.name + ": ")
        + "t " + Math.round(data.main.temp - 273.15) + ", wind " + data.wind.speed + " (OpenWeatherMap)";
}

function getWeather(city, cb) {

  return getData(city);
}

exports.getWeather = getWeather;
