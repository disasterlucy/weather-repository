function processData(data) {
    "use strict";
    var parsedData = JSON.parse(data),
        processedData = 'City: '        + parsedData.name          + '\n'  +
                        'Country: '     + parsedData.sys.country   + '\n'  +
                        'ID: '          + parsedData.sys.id        + '\n'  +
                        'Temperature: ' + parsedData.main.temp     + '°\n' +
                        'Humidity: '    + parsedData.main.humidity + '%\n' +
                        'Wind: '        + parsedData.wind.speed    + 'km/h\n';
    return processedData;
}

function Options (City) {
    this.method = 'GET';
    this.uri = 'http://api.openweathermap.org/data/2.5/weather?q=' + City + '&appid=a0fa1645d2dd0a5850bc73a98585d4e4&units=metric';
}

function getData(City, callback) {
    "use strict";
    var request = require('request-promise');

    request.get(new Options(City))
        .then(function (responce) {
            if(callback)
                callback(null, responce);
        })
        .catch(function (err) {
            callback(err, null);
        });

}

if(module.parent){
    exports.getWeather = getData;
    exports.processData = processData;
} else {
    getData('Kiev');
}
